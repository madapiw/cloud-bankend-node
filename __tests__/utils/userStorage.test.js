const {
  activeUsers,
  addUser,
  removeUser,
} = require("../../src/utils/userStorage");

const EXAMPLE_ID = "qwer1234";
const EXAMPLE_NAME = "test name";

test("Adding user", () => {
  addUser(EXAMPLE_ID, EXAMPLE_NAME);
  expect(activeUsers.length).toEqual(1);
  expect(activeUsers[0]).toMatchObject({ id: EXAMPLE_ID, name: EXAMPLE_NAME });
});

test("Adding existing user", () => {
  expect(() => addUser(EXAMPLE_ID, EXAMPLE_NAME)).toThrow(
    "User already logged in!"
  );
});

test("Removing token", () => {
  removeUser(EXAMPLE_ID);
  expect(activeUsers.length).toEqual(0);
});
