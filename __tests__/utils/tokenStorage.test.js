const {
  addToken,
  checkIfTokenExists,
  removeToken,
  tokenStorage,
} = require("../../src/utils/tokenStorage");

const EXAMPLE_TOKEN = "qwer1234";

test("Adding token", () => {
  addToken(EXAMPLE_TOKEN);
  expect(tokenStorage.length).toEqual(1);
  expect(tokenStorage[0]).toBe(EXAMPLE_TOKEN);
});

test("Adding existing token", () => {
  expect(() => addToken(EXAMPLE_TOKEN)).toThrow("Token already exists!");
});

test("Check if token exists", () => {
  expect(checkIfTokenExists(EXAMPLE_TOKEN)).toBe(true);
  expect(checkIfTokenExists("Asdasdasd")).toBe(false);
});

test("Removing token", () => {
  removeToken(EXAMPLE_TOKEN);
  expect(tokenStorage.length).toEqual(0);
});
