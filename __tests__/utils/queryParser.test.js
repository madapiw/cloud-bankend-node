const {
  arrayQueryParser,
  withoutArrayQueryParser,
  foldersArrayQueryParser,
  foldersWithoutArrayQueryParser,
  likeQueryParser,
  starsQueryParser,
  dayQuery,
  textQuery,
} = require("../../src/utils/queryParser");

const EXAMPLE_ARRAY = ["a", "b", "c"];

test("arrayQueryParser test", () => {
  let query = {};
  query["$and"] = [];
  arrayQueryParser(query, EXAMPLE_ARRAY, "AND", "test");

  expect(query).toMatchObject({
    $and: [
      {
        test: {
          $all: EXAMPLE_ARRAY,
        },
      },
    ],
  });

  query["$and"] = [];
  arrayQueryParser(query, EXAMPLE_ARRAY, "OR", "test");

  expect(query).toMatchObject({
    $and: [
      {
        test: {
          $in: EXAMPLE_ARRAY,
        },
      },
    ],
  });
});

test("withoutArrayQueryParser test", () => {
  let query = {};
  query["$and"] = [];
  withoutArrayQueryParser(query, EXAMPLE_ARRAY, "AND", "test", true);

  expect(query).toMatchObject({
    $and: [
      {
        test: [],
      },
    ],
  });

  query["$and"] = [];
  withoutArrayQueryParser(query, EXAMPLE_ARRAY, "AND", "test", false);

  expect(query).toMatchObject({
    $and: [
      {
        test: {
          $all: EXAMPLE_ARRAY,
        },
      },
    ],
  });
});

test("foldersArrayQueryParser test", () => {
  let query = {};
  query["$and"] = [];
  foldersArrayQueryParser(
    query,
    EXAMPLE_ARRAY,
    "AND",
    true,
    "testNested",
    "test"
  );

  expect(query).toMatchObject({
    $and: [
      {
        testNested: {
          $all: EXAMPLE_ARRAY,
        },
      },
    ],
  });

  query["$and"] = [];
  foldersArrayQueryParser(
    query,
    EXAMPLE_ARRAY,
    "AND",
    false,
    "testNested",
    "test"
  );

  expect(query).toMatchObject({
    $and: [
      {
        test: {
          $all: EXAMPLE_ARRAY,
        },
      },
    ],
  });
});

test("likeQueryParser test", () => {
  let query = {};
  query["$and"] = [];
  likeQueryParser(query, true, false);

  expect(query).toMatchObject({
    $and: [
      {
        like: true,
      },
    ],
  });

  query["$and"] = [];
  likeQueryParser(query, true, true);

  expect(query).toMatchObject({
    $and: [
      {
        like: false,
      },
    ],
  });

  query["$and"] = [];
  likeQueryParser(query, false, true);

  expect(query).toMatchObject({
    $and: [
      {
        like: false,
      },
    ],
  });

  query["$and"] = [];
  likeQueryParser(query, false, false);

  expect(query).toMatchObject({
    $and: [],
  });
});

test("starsQueryParser test", () => {
  let query = {};
  query["$and"] = [];
  starsQueryParser(query, true, 5, false);

  expect(query).toMatchObject({
    $and: [
      {
        stars: 5,
      },
    ],
  });

  query["$and"] = [];
  starsQueryParser(query, true, 0, false);

  expect(query).toMatchObject({
    $and: [
      {
        stars: { $gt: 0 },
      },
    ],
  });

  query["$and"] = [];
  starsQueryParser(query, true, 0, true);

  expect(query).toMatchObject({
    $and: [
      {
        stars: 0,
      },
    ],
  });
});

test("dayQuery test", () => {
  let query = {};
  query["$and"] = [];
  dayQuery(query, true, 5, "day", "$gte");

  expect(query).toMatchObject({
    $and: [
      {
        day: {
          $gte: 5,
        },
      },
    ],
  });

  query["$and"] = [];
  dayQuery(query, false, 5, "day", "$gte");

  expect(query).toMatchObject({
    $and: [],
  });
});

test("textQuery test", () => {
  let query = {};
  query["$and"] = [];
  textQuery(query, "abcd", "text");

  expect(query).toMatchObject({
    $and: [
      {
        text: { $regex: "abcd", $options: "i" },
      },
    ],
  });

  query["$and"] = [];
  textQuery(query, "", "text");

  expect(query).toMatchObject({
    $and: [],
  });
});
