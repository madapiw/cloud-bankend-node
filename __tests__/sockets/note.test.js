const { response } = require("express");
const io = require("socket.io-client");

const {
  setupDatabase,
  shutDownDatabase,
  testLog,
  testFolder,
  testUser,
  testNote,
} = require("../__fixtures__/db");
const { initializeServer, closeServer } = require("../__fixtures__/socketio");

let client;
const port = process.env.PORT || 3000;

beforeAll(async () => {
  await setupDatabase();
  await initializeServer();
  client = await io(`http://localhost:${port}`, {
    query: { user: testUser.name },
    transports: ["websocket"],
    forceNew: true,
    reconnection: false,
  });
});
afterAll(async () => {
  await client.disconnect();
  await shutDownDatabase();
  await closeServer();
});

test("Add new note", (done) => {
  client.emit(
    "note/new",
    {
      text: "Another test note",
      user: testUser._id,
      type: "Log",
      reference: testLog._id,
    },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data).toMatchObject({
        text: "Another test note",
        user: testUser._id.toString(),
        type: "Log",
        reference: testLog._id.toString(),
      });
      done();
    }
  );
});

test("Get single note", (done) => {
  client.emit("note/get", { _id: testNote._id }, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data).toMatchObject({
      text: testNote.text,
      user: testUser._id.toString(),
      type: "Log",
      reference: testLog._id.toString(),
    });
  });
  done();
});

test("Get logs notes", (done) => {
  client.emit("note/getRef", { refId: testLog._id }, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data.length).toEqual(2);
    done();
  });
});

test("Get users notes", (done) => {
  client.emit("note/getUser", { userId: testUser._id }, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data.length).toEqual(2);
    done();
  });
});

test("Update note", (done) => {
  client.emit(
    "note/update",
    { _id: testNote._id, text: "Text after upadte" },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data.text).toBe("Text after upadte");
      done();
    }
  );
});

test("Delete note", (done) => {
  client.emit(
    "note/delete",
    { _id: testNote._id },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      done();
    }
  );
});

test("Check note delete", (done) => {
  client.emit("note/get", { _id: testNote._id });
  client.on("serverError", (response) => {
    expect(response.path).toBe("note/get");
    expect(response.error).toBe("Note not exist!");
    done();
  });
});
