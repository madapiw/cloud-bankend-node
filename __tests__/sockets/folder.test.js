const io = require("socket.io-client");

const {
  setupDatabase,
  shutDownDatabase,
  testPerson,
  testLog,
  testFolder,
  testTag,
  testUser,
} = require("../__fixtures__/db");
const { initializeServer, closeServer } = require("../__fixtures__/socketio");

let client;
const port = process.env.PORT || 3000;

beforeAll(async () => {
  await setupDatabase();
  await initializeServer();
  client = await io(`http://localhost:${port}`, {
    query: { user: testUser.name },
    transports: ["websocket"],
    forceNew: true,
    reconnection: false,
  });
});
afterAll(async () => {
  await client.disconnect();
  await shutDownDatabase();
  await closeServer();
});

test("Add new folder", (done) => {
  client.emit(
    "folder/new",
    { name: "Testing", user: testUser._id },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data).toMatchObject({
        name: "testing",
        user: testUser._id.toString(),
        tags: [],
        persons: [],
        like: false,
        stars: 0,
      });
      expect(response.data.dateStart).toBeUndefined();
      expect(response.data.dateEnd).toBeUndefined();
      done();
    }
  );
});

test("Get single folder", (done) => {
  client.emit("folder/get", { _id: testFolder._id }, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data).toMatchObject({
      name: testFolder.name,
      user: { _id: testUser._id.toString(), name: "Test user" },
      tags: [],
      persons: [],
      like: false,
      stars: 0,
    });
    expect(response.data.dateStart).toBeUndefined();
    expect(response.data.dateEnd).toBeUndefined();
    done();
  });
});

test("Add folder the log", (done) => {
  client.emit(
    "log/addFolder",
    { _id: testLog._id, folderId: testFolder._id },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data.folders[0]).toBe(testFolder._id.toString());
      done();
    }
  );
});

test("Add stars to the folder", (done) => {
  client.emit(
    "folder/stars",
    { _id: testFolder._id, stars: 5 },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data.stars).toEqual(5);
      done();
    }
  );
});

test("Add like to the folder", (done) => {
  client.emit(
    "folder/like",
    { _id: testFolder._id, like: true },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data.like).toBe(true);
      done();
    }
  );
});

test("Add tag to the folder", (done) => {
  client.emit(
    "folder/addTag",
    { _id: testFolder._id, tagId: testTag._id },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data.tags[0]).toBe(testTag._id.toString());
      done();
    }
  );
});

test("Remove tag from the folder", (done) => {
  client.emit(
    "folder/removeTag",
    { _id: testFolder._id, tagId: testTag._id },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data.tags.length).toEqual(0);
      done();
    }
  );
});

test("Add person to the folder", (done) => {
  client.emit(
    "folder/addPerson",
    { _id: testFolder._id, personId: testPerson._id },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data.persons[0]).toBe(testPerson._id.toString());
      done();
    }
  );
});

test("Remove person from the folder", (done) => {
  client.emit(
    "folder/removePerson",
    { _id: testFolder._id, personId: testPerson._id },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data.persons.length).toEqual(0);
      done();
    }
  );
});

test("Remove folder", (done) => {
  client.emit(
    "folder/delete",
    { _id: testFolder._id },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      done();
    }
  );
});

test("Check folder removal", (done) => {
  client.emit("folder/get", { _id: testLog._id });
  client.once("serverError", (data) => {
    expect(data.path).toBe("folder/get");
    expect(data.error).toBe("Folder not exist!");
    done();
  });
});

test("Check folder removal from log", (done) => {
  client.emit("log/get", { _id: testLog._id }, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data.folders.length).toEqual(0);
    done();
  });
});
