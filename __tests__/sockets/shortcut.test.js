const io = require("socket.io-client");

const {
  setupDatabase,
  shutDownDatabase,
  testUser,
  testShortcut,
} = require("../__fixtures__/db");
const { initializeServer, closeServer } = require("../__fixtures__/socketio");

let client;
const port = process.env.PORT || 3000;

beforeAll(async () => {
  await setupDatabase();
  await initializeServer();
  client = await io(`http://localhost:${port}`, {
    query: { user: testUser.name },
    transports: ["websocket"],
    forceNew: true,
    reconnection: false,
  });
});
afterAll(async () => {
  await client.disconnect();
  await shutDownDatabase();
  await closeServer();
});

test("Add new shortcut", (done) => {
  client.emit(
    "shortcut/new",
    { name: "New shortcut", shortcut: "Shift+n" },
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data).toMatchObject({
        name: "New shortcut",
        shortcut: "Shift+n",
        type: "Tag",
      });
      done();
    }
  );
});

test("Add new shortcut fail", (done) => {
  client.emit("shortcut/new", { name: "New shortcut", shortcut: "Shift+n" });
  client.once("serverError", (response) => {
    expect(response.path).toBe("shortcut/new");
    expect(response.data).toMatchObject({
      name: "New shortcut",
      shortcut: "Shift+n",
    });
    done();
  });
});

test("Get all shortcuts", (done) => {
  client.emit("shortcut/getAll", {}, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data.length).toEqual(2);
    done();
  });
});

test("Get types", (done) => {
  client.emit("shortcut/getTypes", {}, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data.length).toEqual(2);
    done();
  });
});

test("Update shortcut", (done) => {
  client.emit(
    "shortcut/update",
    { _id: testShortcut._id, type: "Person", shortcut: "ChangeTestShortcut" },
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data).toMatchObject({
        ...testShortcut,
        _id: testShortcut._id.toString(),
        type: "Person",
        shortcut: "ChangeTestShortcut",
      });
      done();
    }
  );
});

test("Delete shortcut", (done) => {
  client.emit("shortcut/delete", { _id: testShortcut._id });
  client.once("shortcut/getAllUpdate", (data) => {
    expect(data.data.length).toEqual(1);
    done();
  });
});
