const io = require("socket.io-client");

const {
  setupDatabase,
  shutDownDatabase,
  testUser,
} = require("../__fixtures__/db");
const { initializeServer, closeServer } = require("../__fixtures__/socketio");

let client;
const port = process.env.PORT || 3000;

beforeAll(async () => {
  await setupDatabase();
  await initializeServer();
  client = await io(`http://localhost:${port}`, {
    query: { user: testUser.name },
    transports: ["websocket"],
    forceNew: true,
    reconnection: false,
  });
});
afterAll(async () => {
  await client.disconnect();
  await shutDownDatabase();
  await closeServer();
});

test("Get users list", (done) => {
  client.emit("user/getAll", {}, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data.length).toEqual(1);
    expect(response.data[0].name).toBe(testUser.name);
    done();
  });
});

test("Get single user", (done) => {
  client.emit("user/get", { _id: testUser._id }, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data.name).toBe(testUser.name);
    done();
  });
});

test("Add new user", (done) => {
  client.emit(
    "user/new",
    { name: "Test", password: "pass1234" },
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data).toMatchObject({
        name: "Test",
        role: "Logger",
      });

      client.emit("user/getAll", {}, (response2) => {
        expect(response2.status).toBe("ok");
        expect(response2.data.length).toEqual(2);
        done();
      });
    }
  );
});

test("Change password", (done) => {
  client.emit(
    "user/changePassword",
    {
      _id: testUser._id,
      oldPassword: testUser.password,
      newPassword: "NewPass123",
    },
    (response) => {
      expect(response.status).toBe("ok");
      done();
    }
  );
});

test("Change password fail", (done) => {
  client.emit("user/changePassword", {
    _id: testUser._id,
    oldPassword: testUser.password,
    newPassword: "NewPass123",
  });
  client.once("serverError", (data) => {
    expect(data.error).toBe("Wrong password!");
    expect(data.path).toBe("user/changePassword");
    done();
  });
});

test("Change role", (done) => {
  client.emit(
    "user/changeRole",
    {
      _id: testUser._id,
      newRole: "Logger",
    },
    (response) => {
      expect(response.status).toBe("ok");
      done();
    }
  );
});

test("Change role fail", (done) => {
  client.emit("user/changeRole", {
    _id: testUser._id,
    newRole: "adasdasd",
  });
  client.once("serverError", (data) => {
    expect(data.error).not.toBeUndefined();
    done();
  });
});

test("Delete user", (done) => {
  client.emit(
    "user/delete",
    {
      _id: testUser._id,
    },
    (response) => {
      expect(response.status).toBe("ok");

      client.emit("user/getAll", {}, (response2) => {
        expect(response2.status).toBe("ok");
        expect(response2.data.length).toEqual(1);
        done();
      });
    }
  );
});
