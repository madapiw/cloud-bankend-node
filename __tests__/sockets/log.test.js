const io = require("socket.io-client");

const {
  setupDatabase,
  shutDownDatabase,
  testPerson,
  testLog,
  testFolder,
  testTag,
  testUser,
} = require("../__fixtures__/db");
const { initializeServer, closeServer } = require("../__fixtures__/socketio");

let client;
const port = process.env.PORT || 3000;

beforeAll(async () => {
  await setupDatabase();
  await initializeServer();
  client = await io(`http://localhost:${port}`, {
    query: { user: testUser.name },
    transports: ["websocket"],
    forceNew: true,
    reconnection: false,
  });
});
afterAll(async () => {
  await client.disconnect();
  await shutDownDatabase();
  await closeServer();
});

test("Add new log", (done) => {
  client.emit(
    "log/new",
    { text: "Testing", user: testUser._id },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data).toMatchObject({
        text: "Testing",
        user: testUser._id.toString(),
        tags: [],
        persons: [],
        folders: [],
        like: false,
        stars: 0,
      });
      expect(response.data.dateStart).not.toBeUndefined();
      expect(response.data.dateEnd).not.toBeUndefined();
      done();
    }
  );
});

test("Get single log", (done) => {
  client.emit("log/get", { _id: testLog._id }, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data).toMatchObject({
      text: testLog.text,
      user: { _id: testUser._id.toString(), name: testUser.name },
      tags: [],
      persons: [],
      folders: [],
      like: false,
      stars: 0,
    });
    expect(response.data.dateStart).not.toBeUndefined();
    expect(response.data.dateEnd).not.toBeUndefined();
    done();
  });
});

test("Add stars to the log", (done) => {
  client.emit(
    "log/stars",
    { _id: testLog._id, stars: 5 },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data.stars).toEqual(5);
      done();
    }
  );
});

test("Add like to the log", (done) => {
  client.emit(
    "log/like",
    { _id: testLog._id, like: true },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data.like).toBe(true);
      done();
    }
  );
});

test("Add folder the log", (done) => {
  client.emit(
    "log/addFolder",
    { _id: testLog._id, folderId: testFolder._id },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data.folders[0]).toBe(testFolder._id.toString());
      done();
    }
  );
});

test("Remove folder the log", (done) => {
  client.emit(
    "log/removeFolder",
    { _id: testLog._id, folderId: testFolder._id },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data.folders.length).toEqual(0);
      done();
    }
  );
});

test("Update log", (done) => {
  client.emit(
    "log/update",
    { _id: testLog._id, tags: [testTag._id], persons: [testPerson._id] },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data.tags[0]).toBe(testTag._id.toString());
      expect(response.data.persons[0]).toBe(testPerson._id.toString());
      done();
    }
  );
});

test("Remove log", (done) => {
  client.emit("log/delete", { _id: testLog._id }, "TestID1234", (response) => {
    expect(response.status).toBe("ok");
    done();
  });
});

test("Check log removal", (done) => {
  client.emit("log/get", { _id: testLog._id });
  client.once("serverError", (data) => {
    expect(data.path).toBe("log/get");
    expect(data.error).toBe("Log not exist!");
    done();
  });
});
