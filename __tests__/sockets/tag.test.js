const io = require("socket.io-client");

const {
  setupDatabase,
  shutDownDatabase,
  testTag,
  testLog,
  testFolder,
  testUser,
} = require("../__fixtures__/db");
const { initializeServer, closeServer } = require("../__fixtures__/socketio");

let client;
const port = process.env.PORT || 3000;

beforeAll(async () => {
  await setupDatabase();
  await initializeServer();
  client = await io(`http://localhost:${port}`, {
    query: { user: testUser.name },
    transports: ["websocket"],
    forceNew: true,
    reconnection: false,
  });
});
afterAll(async () => {
  await client.disconnect();
  await shutDownDatabase();
  await closeServer();
});

test("Add new tag", (done) => {
  client.emit("tag/new", { name: "Add", type: "adding" }, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data).toMatchObject({
      name: "add",
      type: "adding",
      show: true,
      color: "#fff",
    });
    done();
  });
});

test("Add new tag fail", (done) => {
  client.emit("tag/new", { name: "Add", type: "adding" });
  client.once("serverError", (response) => {
    expect(response.path).toBe("tag/new");
    expect(response.data).toMatchObject({ name: "Add", type: "adding" });
    done();
  });
});

test("Get single tag", (done) => {
  client.emit("tag/get", { _id: testTag._id }, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data).toMatchObject({
      ...testTag,
      _id: testTag._id.toString(),
    });
    done();
  });
});

test("Get all tag", (done) => {
  client.emit("tag/getAll", {}, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data.length).toEqual(2);
    done();
  });
});

test("Get get types", (done) => {
  client.emit("tag/getTypes", {}, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data.length).toEqual(2);
    done();
  });
});

test("Update tag", (done) => {
  client.emit("tag/update", { _id: testTag._id, show: false }, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data).toMatchObject({
      ...testTag,
      _id: testTag._id.toString(),
      show: false,
    });
    done();
  });
});

test("Add tag to the log", (done) => {
  client.emit(
    "log/update",
    { _id: testLog._id, tags: [testTag._id] },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data.tags.length).toEqual(1);
      done();
    }
  );
});

test("Add tag to the folder", (done) => {
  client.emit(
    "folder/update",
    { _id: testFolder._id, tags: [testTag._id] },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data.tags.length).toEqual(1);
      done();
    }
  );
});

test("Delete tag", (done) => {
  client.emit("tag/delete", { _id: testTag._id });
  client.once("tag/getAllUpdate", (data) => {
    expect(data.data.length).toEqual(1);
    done();
  });
});

test("Check tag removal from log", (done) => {
  client.emit("log/get", { _id: testLog._id }, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data.tags.length).toEqual(0);
    done();
  });
});

test("Check tag removal from folder", (done) => {
  client.emit("folder/get", { _id: testFolder._id }, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data.tags.length).toEqual(0);
    done();
  });
});
