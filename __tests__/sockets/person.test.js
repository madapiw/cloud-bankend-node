const io = require("socket.io-client");

const {
  setupDatabase,
  shutDownDatabase,
  testPerson,
  testLog,
  testFolder,
  testUser,
} = require("../__fixtures__/db");
const { initializeServer, closeServer } = require("../__fixtures__/socketio");

let client;
const port = process.env.PORT || 3000;

beforeAll(async () => {
  await setupDatabase();
  await initializeServer();
  client = await io(`http://localhost:${port}`, {
    query: { user: testUser.name },
    transports: ["websocket"],
    forceNew: true,
    reconnection: false,
  });
});
afterAll(async () => {
  await client.disconnect();
  await shutDownDatabase();
  await closeServer();
});

test("Add new person", (done) => {
  client.emit("person/new", { name: "Add", type: "adding" }, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data).toMatchObject({
      name: "add",
      type: "adding",
      show: true,
      photo: "",
    });
    done();
  });
});

test("Add new person fail", (done) => {
  client.emit("person/new", { name: "Add", type: "adding" });
  client.once("serverError", (response) => {
    expect(response.path).toBe("person/new");
    expect(response.data).toMatchObject({ name: "Add", type: "adding" });
    done();
  });
});

test("Get single person", (done) => {
  client.emit("person/get", { _id: testPerson._id }, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data).toMatchObject({
      ...testPerson,
      _id: testPerson._id.toString(),
      show: true,
      photo: "",
    });
    done();
  });
});

test("Get all persons", (done) => {
  client.emit("person/getAll", {}, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data.length).toEqual(2);
    done();
  });
});

test("Get types", (done) => {
  client.emit("person/getTypes", {}, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data.length).toEqual(2);
    done();
  });
});

test("Update person", (done) => {
  client.emit(
    "person/update",
    { _id: testPerson._id, show: false },
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data).toMatchObject({
        ...testPerson,
        _id: testPerson._id.toString(),
        show: false,
      });
      done();
    }
  );
});

test("Add person to the log", (done) => {
  client.emit(
    "log/update",
    { _id: testLog._id, persons: [testPerson._id] },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data.persons.length).toEqual(1);
      done();
    }
  );
});

test("Add person to the folder", (done) => {
  client.emit(
    "folder/update",
    { _id: testFolder._id, persons: [testPerson._id] },
    "TestID1234",
    (response) => {
      expect(response.status).toBe("ok");
      expect(response.data.persons.length).toEqual(1);
      done();
    }
  );
});

test("Delete person", (done) => {
  client.emit("person/delete", { _id: testPerson._id });
  client.once("person/getAllUpdate", (data) => {
    expect(data.data.length).toEqual(1);
    done();
  });
});

test("Check person removal from log", (done) => {
  client.emit("log/get", { _id: testLog._id }, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data.persons.length).toEqual(0);
    done();
  });
});

test("Check person removal from folder", (done) => {
  client.emit("folder/get", { _id: testFolder._id }, (response) => {
    expect(response.status).toBe("ok");
    expect(response.data.persons.length).toEqual(0);
    done();
  });
});
