const request = require("supertest");
const app = require("../../src/network/express");
const { tokenStorage } = require("../../src/utils/tokenStorage");
const {
  setupDatabase,
  shutDownDatabase,
  testUser,
} = require("../__fixtures__/db");

beforeAll(setupDatabase);
afterAll(shutDownDatabase);

test("Get users", async () => {
  const response = await request(app).get("/users").expect(200);

  expect(response.body.status).toBe("ok");
  expect(response.body.data.length).toEqual(1);
});

test("Login test", async () => {
  const response = await request(app)
    .post("/login")
    .send({ name: testUser.name, password: testUser.password })
    .expect(200);

  expect(response.body.status).toBe("ok");
  expect(response.body.data.token).not.toBeNull();
  expect(response.body.data.role).toBe("Admin");
  expect(tokenStorage[0]).toEqual(response.body.data.token);
});

test("Login fail", async () => {
  const response = await request(app)
    .post("/login")
    .send({ name: testUser.name, password: "asdasdasd" })
    .expect(200);

  expect(response.body.status).toBe("error");
  expect(response.body.error).toBe("Wrong password");
});
