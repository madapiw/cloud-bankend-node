const mongoose = require("mongoose");

const User = require("../../src/models/user");
const Tag = require("../../src/models/tag");
const Person = require("../../src/models/person");
const Log = require("../../src/models/log");
const Folder = require("../../src/models/folder");
const Note = require("../../src/models/note");
const Shortcut = require("../../src/models/shortcut");

const testUserId = new mongoose.Types.ObjectId();
const testTagId = new mongoose.Types.ObjectId();
const testPersonId = new mongoose.Types.ObjectId();
const testLogId = new mongoose.Types.ObjectId();
const testFolderId = new mongoose.Types.ObjectId();
const testNoteId = new mongoose.Types.ObjectId();
const testShortcutId = new mongoose.Types.ObjectId();

const testUser = {
  _id: testUserId,
  name: "Test user",
  password: "test@123",
  role: "Admin",
};

const testTag = {
  _id: testTagId,
  name: "testtag",
  type: "test",
  color: "#232323",
};

const testPerson = {
  _id: testPersonId,
  name: "test person",
  type: "contestant",
};

const testLog = {
  _id: testLogId,
  text: "Test log",
  user: testUserId,
};

const testFolder = { _id: testFolderId, name: "test folder", user: testUserId };

const testNote = {
  _id: testNoteId,
  text: "Test note",
  user: testUserId,
  type: "Log",
  reference: testLogId,
};

const testShortcut = {
  _id: testShortcutId,
  name: "exit",
  shortcut: "Alt+F4",
};

const setupDatabase = async () => {
  require("../../src/db/mongoose");

  await User.deleteMany({});
  await Tag.deleteMany({});
  await Person.deleteMany({});
  await Folder.deleteMany({});
  await Log.deleteMany({});
  await Note.deleteMany({});
  await Shortcut.deleteMany({});
  await new User(testUser).save();
  await new Tag(testTag).save();
  await new Person(testPerson).save();
  await new Log(testLog).save();
  await new Folder(testFolder).save();
  await new Note(testNote).save();
  await new Shortcut(testShortcut).save();
};

const shutDownDatabase = () => {
  mongoose.connection.close();
};

module.exports = {
  testUser,
  testTag,
  testPerson,
  testLog,
  testFolder,
  testNote,
  testShortcut,
  setupDatabase,
  shutDownDatabase,
};
