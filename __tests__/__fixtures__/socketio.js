const server = require("../../src/network/socketio");

const initializeServer = async () => {
  const port = process.env.PORT || 3000;
  await server.listen(port);
};

const closeServer = async () => {
  if (server.listening) {
    await server.close();
  }
};

module.exports = { initializeServer, closeServer };
