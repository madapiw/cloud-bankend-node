process.env.NODE_ENV = "PROD";
process.env.TOKEN_SECRET = "iwuefhw9e78few";
process.env.LICENSES_NUMBER = 5;

process.env.PORT = 6925;
process.env.MONGODB_URL =
  "mongodb://mo14689_adamtest:!Wall123@mongo1.mydevil.net/mo14689_adamtest";
process.env.REFERENCE_DATE = "2021-05-06";
process.env.CLOUD_ADDRESS = "inventory.modeon.usermd.net";

//app start

require("./src/db/mongoose");

const { infoLogger } = require("./src/logger/logger");
const server = require("./src/network/socketio");
const createAdminUser = require("./src/utils/createAdminUser");

const port = process.env.PORT || 6925;

server.listen(port, () => {
  console.log("Server is up on port: ", port);
  infoLogger.info(`Server is up on port: ${port}`);
  createAdminUser();
});
