//storage of connected users
const activeUsers = [];

const addUser = (id, name) => {
  //validate if data exists
  if (!id || !name) {
    throw new Error("No id / name provided");
  }

  //check if user is already logged in
  const isActive = activeUsers.find((elem) => elem.name === name);
  if (isActive) {
    throw new Error("User already logged in!");
  }

  const newUser = { id, name };
  activeUsers.push(newUser);
};

const removeUser = (id) => {
  const index = activeUsers.findIndex((user) => user.id === id);

  if (index !== -1) {
    return activeUsers.splice(index, 1)[0];
  }
};

module.exports = { activeUsers, addUser, removeUser };
