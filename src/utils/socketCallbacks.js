const { errorLogger } = require("../logger/logger");

const dataCallback = (callback, data, messageId = null) => {
  callback({
    status: "ok",
    data: data,
    ...(messageId && { messageId: messageId }),
  });
};

const noDataCallback = (callback, messageId = null) => {
  callback({
    status: "ok",
    ...(messageId && { messageId: messageId }),
  });
};

const errorCallback = (socket, path, data, error, messageId = null) => {
  socket.emit("serverError", {
    path: path,
    data: data,
    error: error.message,
    ...(messageId && { messageId: messageId }),
  });
  errorLogger.error({
    path: path,
    data: data,
    error: error.message,
  });
};

module.exports = {
  dataCallback,
  noDataCallback,
  errorCallback,
};
