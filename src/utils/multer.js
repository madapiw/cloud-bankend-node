const multer = require("multer");

const storage = multer.diskStorage({
  filename(req, file, callback) {
    callback(null, req.body.photoName);
  },
  destination: "../public_html/",
});

const upload = multer({
  limits: {
    fileSize: 2000000,
  },

  fileFilter(req, file, callback) {
    if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
      return callback(new Error("Please, upload an image!"));
    }

    callback(null, true);
  },
  storage: storage,
});

module.exports = upload;
