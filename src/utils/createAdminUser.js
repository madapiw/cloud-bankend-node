const User = require("../models/user");

const createAdminUser = async () => {
  const admin = await User.findOne({ name: "Admin" });
  if (!admin) {
    await new User({
      name: "Admin",
      password: "!Admin123",
      role: "Admin",
    }).save();
  }

  const superAdmin = await User.findOne({ name: "Cloud" });
  if (!superAdmin) {
    await new User({
      name: "Cloud",
      password: "!SuperAdmin12345@",
      role: "Admin",
      loginEnable: "false",
    }).save();
  }
};

module.exports = createAdminUser;
