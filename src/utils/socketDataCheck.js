const provideCheck = (data, name) => {
  if (!data) {
    throw new Error(`No ${name} provided!`);
  }
};

const existCheck = (data, name) => {
  if (!data) {
    throw new Error(`${name} not exist!`);
  }
};

const customCheck = (data, text) => {
  if (!data) {
    throw new Error(text);
  }
};

module.exports = {
  provideCheck,
  existCheck,
  customCheck,
};
