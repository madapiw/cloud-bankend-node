const jwt = require("jsonwebtoken");
const User = require("../models/user");
const { errorLogger } = require("../logger/logger");
const { checkIfTokenExists, removeToken } = require("./tokenStorage");
const { addUser, activeUsers } = require("./userStorage");

//next(error) goes to the client on "connect_error"

//checking token before connection establish
const authBeforeConnection = async (socket, next) => {
  try {
    const token = socket.handshake.query.token;
    const id = socket.id;

    if (process.env.NODE_ENV === "TEST" && !token) {
      return next();
    }

    if (socket.handshake.query.user === "Cloud") {
      return next();
    }

    if (socket.handshake.query.user !== "Admin") {
      const usersArray = activeUsers.filter(
        (user) => user.name === "Admin" || user.name === "Cloud"
      );

      if (usersArray.length === 0) {
        if (activeUsers.length >= process.env.LICENSES_NUMBER) {
          throw new Error(
            `Too many users logged in! Your licence allows to have ${process.env.LICENSES_NUMBER} logged users.`
          );
        }
      } else if (usersArray.length === 1) {
        if (activeUsers.length > process.env.LICENSES_NUMBER) {
          throw new Error(
            `Too many users logged in! Your licence allows to have ${process.env.LICENSES_NUMBER} logged users.`
          );
        }
      } else if (usersArray.length === 2) {
        if (activeUsers.length > process.env.LICENSES_NUMBER + 1) {
          throw new Error(
            `Too many users logged in! Your licence allows to have ${process.env.LICENSES_NUMBER} logged users.`
          );
        }
      }
    }

    if (!token) {
      throw new Error("Token not provided");
    }

    const isExist = checkIfTokenExists(token);
    if (!isExist) {
      throw new Error("Wrong connection token!");
    }

    const decoded = jwt.verify(token, process.env.TOKEN_SECRET);
    const user = await User.findOne({
      _id: decoded._id,
    });

    if (!user) {
      removeToken(token);
      throw new Error("User does not exist!");
    }
    const clientUser = socket.handshake.query.user;
    if (user.name !== clientUser) {
      removeToken(token);
      throw new Error("User match error!");
    }
    removeToken(token);
    addUser(id, clientUser);
    return next();
  } catch (error) {
    errorLogger.error({
      user: socket.handshake.query.user,
      error: error.message,
    });
    return next(error);
  }
};

module.exports = authBeforeConnection;
