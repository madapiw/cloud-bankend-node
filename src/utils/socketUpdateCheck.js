const { get } = require("lodash");

const updateCheck = (toSave, data, name, defaultVal = null) => {
  const value = get(data, name, defaultVal);
  if (value !== null) {
    toSave[name] = value;
  }
};

module.exports = {
  updateCheck,
};
