const arrayQueryParser = (query, arrayFilters, filtersMode, fieldToCheck) => {
  if (arrayFilters.length > 0) {
    const mode = filtersMode === "AND" ? "$all" : "$in";
    const length = query["$and"].push({ [fieldToCheck]: {} });
    query["$and"][length - 1][fieldToCheck][mode] = arrayFilters;
  }
};

const withoutArrayQueryParser = (
  query,
  arrayFilters,
  filtersMode,
  fieldToCheck,
  withoutFilter,
  onlyMode = false
) => {
  if (onlyMode) {
    onlyArrayQueryParser(query, arrayFilters, fieldToCheck);
  } else if (withoutFilter) {
    query["$and"].push({ [fieldToCheck]: [] });
  } else {
    arrayQueryParser(query, arrayFilters, filtersMode, fieldToCheck);
  }
};

const onlyArrayQueryParser = (query, arrayFilters, fieldToCheck) => {
  query["$and"].push({ [fieldToCheck]: { $size: 1, $in: [...arrayFilters] } });
};

const foldersArrayQueryParser = (
  query,
  arrayFilters,
  filtersMode,
  searchForNested,
  nestedFieldToCheck,
  fieldToCheck
) => {
  if (arrayFilters.length > 0) {
    const mode = filtersMode === "AND" ? "$all" : "$in";
    const lookingFor = searchForNested ? nestedFieldToCheck : fieldToCheck;
    const length = searchForNested
      ? query["$and"].push({ [nestedFieldToCheck]: {} })
      : query["$and"].push({ [fieldToCheck]: {} });
    query["$and"][length - 1][lookingFor][mode] = arrayFilters;
  }
};

const foldersWithoutArrayQueryParser = (
  query,
  arrayFilters,
  filtersMode,
  searchForNested,
  nestedFieldToCheck,
  fieldToCheck,
  withoutFilter
) => {
  if (withoutFilter) {
    query["$and"].push({ [fieldToCheck]: [] });
  } else {
    foldersArrayQueryParser(
      query,
      arrayFilters,
      filtersMode,
      searchForNested,
      nestedFieldToCheck,
      fieldToCheck
    );
  }
};

const likeQueryParser = (query, likeFilter, withoutLikes) => {
  if (withoutLikes) {
    query["$and"].push({ like: false });
  } else {
    if (likeFilter) {
      query["$and"].push({ like: true });
    }
  }
};

const starsQueryParser = (query, starsFilter, numberOfStars, withoutStars) => {
  if (withoutStars) {
    query["$and"].push({ stars: 0 });
  } else {
    if (starsFilter) {
      if (numberOfStars === 0) {
        query["$and"].push({ stars: { $gt: 0 } });
      } else {
        query["$and"].push({ stars: numberOfStars });
      }
    }
  }
};

const dayQuery = (
  query,
  dayStartFilter,
  dayStartValue,
  fieldToCheck,
  fieldCondition
) => {
  if (dayStartFilter) {
    query["$and"].push({ [fieldToCheck]: { [fieldCondition]: dayStartValue } });
  }
};

const textQuery = (query, textFilter, fieldToCheck) => {
  if (textFilter) {
    query["$and"].push({
      [fieldToCheck]: { $regex: textFilter, $options: "i" },
    });
  }
};

const logsQueryParser = (data) => {
  const {
    personsFilters,
    personsMode,
    onlyPerson,
    tagsFilters,
    tagsMode,
    onlyTag,
    usersFilters,
    usersMode,
    foldersFilters,
    foldersMode,
    likeFilter,
    starsFilter,
    numberOfStars,
    dayStartFilter,
    dayStartValue,
    dayEndFilter,
    dayEndValue,
    withoutTags,
    withoutPersons,
    withoutFolders,
    withoutStars,
    withoutLikes,
    textFilter,
  } = data;
  let query = {};
  query["$and"] = [];

  withoutArrayQueryParser(
    query,
    personsFilters,
    personsMode,
    "persons",
    withoutPersons,
    onlyPerson
  );
  withoutArrayQueryParser(
    query,
    tagsFilters,
    tagsMode,
    "tags",
    withoutTags,
    onlyTag
  );
  arrayQueryParser(query, usersFilters, usersMode, "user");
  withoutArrayQueryParser(
    query,
    foldersFilters,
    foldersMode,
    "folders",
    withoutFolders
  );
  likeQueryParser(query, likeFilter, withoutLikes);
  starsQueryParser(query, starsFilter, numberOfStars, withoutStars);
  dayQuery(query, dayStartFilter, dayStartValue, "day", "$gte");
  dayQuery(query, dayEndFilter, dayEndValue, "day", "$lte");
  textQuery(query, textFilter, "text");

  if (query["$and"].length === 0) {
    return {};
  }

  return query;
};

const foldersQueryParser = (data) => {
  const {
    personsFilters,
    personsMode,
    onlyPerson,
    tagsFilters,
    tagsMode,
    onlyTag,
    usersFilters,
    usersMode,
    likeFilter,
    starsFilter,
    numberOfStars,
    dayStartFilter,
    dayStartValue,
    dayEndFilter,
    dayEndValue,
    withoutTags,
    withoutPersons,
    withoutStars,
    withoutLikes,
    textFilter,
    searchForNestedTagsAndPersons,
  } = data;

  let query = {};
  query["$and"] = [];

  foldersWithoutArrayQueryParser(
    query,
    personsFilters,
    personsMode,
    searchForNestedTagsAndPersons,
    "logsPersons",
    "persons",
    withoutPersons,
    onlyPerson
  );
  foldersWithoutArrayQueryParser(
    query,
    tagsFilters,
    tagsMode,
    searchForNestedTagsAndPersons,
    "logsTags",
    "tags",
    withoutTags,
    onlyTag
  );
  arrayQueryParser(query, usersFilters, usersMode, "user");
  likeQueryParser(query, likeFilter, withoutLikes);
  starsQueryParser(query, starsFilter, numberOfStars, withoutStars);
  dayQuery(query, dayStartFilter, dayStartValue, "dayStart", "$gte");
  dayQuery(query, dayEndFilter, dayEndValue, "dayEnd", "$lte");
  textQuery(query, textFilter, "name");

  if (query["$and"].length === 0) {
    return {};
  }

  return query;
};

module.exports = {
  logsQueryParser,
  foldersQueryParser,
  arrayQueryParser,
  withoutArrayQueryParser,
  foldersArrayQueryParser,
  foldersWithoutArrayQueryParser,
  likeQueryParser,
  starsQueryParser,
  dayQuery,
  textQuery,
};
