//storage of created tokens
const tokenStorage = [];

const checkIfTokenExists = (token) => {
  const isExisting = tokenStorage.find((elem) => elem === token);
  if (isExisting) {
    return true;
  }

  return false;
};

const addToken = (token) => {
  //check if token already exist
  const isExisting = tokenStorage.find((elem) => elem === token);
  if (isExisting) {
    throw new Error("Token already exists!");
  }
  //add deleting token after 5 minutes
  tokenStorage.push(token);
};

const removeToken = (token) => {
  const index = tokenStorage.findIndex((elem) => elem === token);

  if (index !== -1) {
    return tokenStorage.splice(index, 1)[0];
  }
};

module.exports = { tokenStorage, checkIfTokenExists, addToken, removeToken };
