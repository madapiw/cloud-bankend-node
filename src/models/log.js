const mongoose = require("mongoose");

const logSchema = new mongoose.Schema({
  text: {
    type: String,
    required: true,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  dateStart: {
    type: Date,
    default: new Date(),
  },
  day: {
    type: Number,
  },
  dateEnd: {
    type: Date,
    default: new Date(),
  },
  tags: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Tag",
    },
  ],
  persons: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Person",
    },
  ],
  folders: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Folder",
    },
  ],
  like: {
    type: Boolean,
    default: false,
  },
  stars: {
    type: Number,
    default: 0,
    enum: [0, 1, 2, 3, 4, 5],
  },
});

logSchema.virtual("notes", {
  ref: "Note",
  localField: "_id",
  foreignField: "reference",
});

//premethods
logSchema.pre("save", function (next) {
  const newLog = this;
  const logDate = new Date(newLog.dateStart);
  const refDate = new Date(process.env.REFERENCE_DATE);

  const day = Math.floor((logDate - refDate) / (3600 * 24 * 1000)) + 1;

  newLog.day = day;

  next();
});

const Log = mongoose.model("Log", logSchema);
module.exports = Log;
