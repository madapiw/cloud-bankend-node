const mongoose = require("mongoose");
const Log = require("./log");

const folderSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true,
      lowercase: true,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    dateStart: {
      type: Date,
      default: undefined,
    },
    dayStart: {
      type: Number,
    },
    dateEnd: {
      type: Date,
      default: undefined,
    },
    dayEnd: {
      type: Number,
    },
    tags: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Tag",
      },
    ],
    logsTags: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Tag",
      },
    ],
    persons: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Person",
      },
    ],
    logsPersons: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Person",
      },
    ],
    like: {
      type: Boolean,
      default: false,
    },
    stars: {
      type: Number,
      default: 0,
      enum: [0, 1, 2, 3, 4, 5],
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

folderSchema.virtual("logs", {
  ref: "Log",
  localField: "_id",
  foreignField: "folders",
});

folderSchema.virtual("notes", {
  ref: "Note",
  localField: "_id",
  foreignField: "reference",
});

//premethods
folderSchema.pre("deleteOne", async function (next) {
  const id = this.getFilter()["_id"];

  await Log.updateMany(
    {
      folders: id,
    },
    {
      $pull: { folders: id },
    }
  );

  next();
});

const Folder = mongoose.model("Folder", folderSchema);
module.exports = Folder;
