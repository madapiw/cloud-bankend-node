const mongoose = require("mongoose");
const validator = require("validator");
const Log = require("./log");
const Folder = require("./folder");

const tagSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  type: {
    type: String,
    required: true,
    lowercase: true,
  },
  show: {
    type: Boolean,
    default: true,
  },
  color: {
    type: String,
    default: "#fff",
    validate(value) {
      if (!validator.isHexColor(value)) {
        throw new Error("Wrong color value!");
      }
    },
  },
});

tagSchema.static("getTypes", async () => {
  const types = await Tag.distinct("type");
  return types;
});

tagSchema.pre("deleteOne", async function (next) {
  const id = this.getFilter()["_id"];

  await Log.updateMany(
    {
      tags: id,
    },
    {
      $pull: { tags: id },
    }
  );
  await Folder.updateMany(
    {
      tags: id,
    },
    {
      $pull: { tags: id },
    }
  );

  next();
});

const Tag = mongoose.model("Tag", tagSchema);
module.exports = Tag;
