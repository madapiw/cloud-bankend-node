const mongoose = require("mongoose");

const TYPES = ["Log", "Folder"];

const noteSchema = new mongoose.Schema({
  text: {
    type: String,
    required: true,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  date: {
    type: Date,
    default: new Date(),
  },
  type: {
    type: String,
    required: true,
    enum: TYPES,
  },
  reference: {
    type: mongoose.Schema.Types.ObjectId,
    refPath: "type",
  },
});

const Note = mongoose.model("Note", noteSchema);
module.exports = Note;
