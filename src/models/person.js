const mongoose = require("mongoose");
const Log = require("./log");
const Folder = require("./folder");

const personSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  type: {
    type: String,
    required: true,
    lowercase: true,
  },
  show: {
    type: Boolean,
    default: true,
  },
  photo: {
    type: String,
    default: "",
  },
});

personSchema.static("getTypes", async () => {
  const types = await Person.distinct("type");
  return types;
});

personSchema.pre("deleteOne", async function (next) {
  const id = this.getFilter()["_id"];

  await Log.updateMany(
    {
      persons: id,
    },
    {
      $pull: { persons: id },
    }
  );
  await Folder.updateMany(
    {
      persons: id,
    },
    {
      $pull: { persons: id },
    }
  );

  next();
});

const Person = mongoose.model("Person", personSchema);
module.exports = Person;
