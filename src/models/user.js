const mongoose = require("mongoose");
const bcrytp = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { addToken } = require("../utils/tokenStorage");

const ROLES = ["Logger", "Editor", "Admin"];

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
    minlength: 3,
    maxlength: 25,
  },
  password: {
    type: String,
    required: true,
    minlength: 8,
    maxlength: 64,
  },
  role: {
    type: String,
    default: "Logger",
    enum: ROLES,
  },
  loginEnable: {
    type: Boolean,
    default: true,
  },
});

//statics
userSchema.static("loginUser", async (name, password) => {
  const user = await User.findOne({ name: name });

  if (!user) {
    throw new Error("User not found");
  }
  const correctPassword = await bcrytp.compare(password, user.password);

  if (!correctPassword) {
    throw new Error("Wrong password");
  }

  return user;
});

userSchema.static("getRoles", () => ROLES);

//methods
userSchema.method("checkPassword", async function (password) {
  const user = this;

  const correctPassword = await bcrytp.compare(password, user.password);

  if (!correctPassword) {
    return false;
  }
  return true;
});

userSchema.method("generateToken", async function () {
  const user = this;

  const token = jwt.sign(
    { _id: user._id.toString() },
    process.env.TOKEN_SECRET
  );

  addToken(token);

  return token;
});

//premethods
userSchema.pre("save", async function (next) {
  const user = this;

  if (user.isModified("password")) {
    user.password = await bcrytp.hash(user.password, 8);
  }

  next();
});

const User = mongoose.model("User", userSchema);

module.exports = User;
