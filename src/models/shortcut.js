const mongoose = require("mongoose");

const TYPES = ["Tag", "Person"];

const shortcutSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
  },
  shortcut: {
    type: String,
    unique: true,
    default: "",
  },
  type: {
    type: String,
    default: "Tag",
    enum: TYPES,
  },
});

//statics
shortcutSchema.static("getTypes", () => TYPES);

const Shortcut = mongoose.model("Shortcut", shortcutSchema);

module.exports = Shortcut;
