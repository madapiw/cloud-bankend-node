const express = require("express");
const path = require("path");
const Person = require("../models/person");
const uploadPhoto = require("../utils/multer");
const { provideCheck, existCheck } = require("../utils/socketDataCheck");

const router = new express.Router();

router.post("/person/photo", uploadPhoto.single("photo"), async (req, res) => {
  try {
    const id = req.body._id;
    const photoName = req.body.photoName;
    provideCheck(id, "Person id");
    provideCheck(photoName, "Photo name");

    const person = await Person.findById(id);
    existCheck(person, "Person");

    person.photo = process.env.CLOUD_ADDRESS + "/" + req.body.photoName;
    await person.save();
    res.send({ status: "ok" });
  } catch (error) {
    res.send({ status: "error", error: error.message });
  }
});

module.exports = router;
