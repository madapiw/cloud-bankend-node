const express = require("express");
const User = require("../models/user");
const { existCheck } = require("../utils/socketDataCheck");

const router = new express.Router();

//get all users
router.get("/users", async (req, res) => {
  try {
    const users = await User.find({ loginEnable: true }).select("name role");

    res.send({ status: "ok", data: users });
  } catch (error) {
    res.send({ status: "error", error: error.message });
  }
});
// user should run this endpoint on reconnect event: socket.on("reconnect", () => askForToken), name and password should be kept in memory
router.post("/login", async (req, res) => {
  try {
    const user = await User.loginUser(req.body.name, req.body.password);
    existCheck(user, "User");

    const token = await user.generateToken();

    res.send({
      status: "ok",
      data: {
        token: token,
        role: user.role,
        id: user.id,
        refDate: process.env.REFERENCE_DATE,
      },
    });
  } catch (error) {
    res.send({ status: "error", error: error.message });
  }
});

module.exports = router;
