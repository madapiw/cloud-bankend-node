const path = require("path");
const winston = require("winston");

const infoLogger = winston.createLogger({
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json()
  ),
  level: "info",
  transports: [
    new winston.transports.File({
      filename: path.join(__dirname, "../../logs/info.log"),
      level: "info",
      maxsize: 5242880, //5MB
      maxFiles: 10,
    }),
  ],
});

const errorLogger = winston.createLogger({
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json()
  ),
  level: "error",
  transports: [
    new winston.transports.File({
      filename: path.join(__dirname, "../../logs/app-error.log"),
      level: "error",
      maxsize: 5242880, //5MB
      maxFiles: 10,
    }),
  ],
});

module.exports = { infoLogger, errorLogger };
