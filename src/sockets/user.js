const User = require("../models/user");

const {
  dataCallback,
  errorCallback,
  noDataCallback,
} = require("../utils/socketCallbacks");
const {
  provideCheck,
  existCheck,
  customCheck,
} = require("../utils/socketDataCheck");

const socketUserHandler = (io, socket) => {
  socket.on("user/getAll", async (data, callback) => {
    try {
      const users = await User.find({}).select("name role");

      dataCallback(callback, users);
    } catch (error) {
      errorCallback(socket, "user/getAll", {}, error);
    }
  });

  socket.on("user/get", async (data, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const user = await User.findById(id).select("name role");

      dataCallback(callback, user);
    } catch (error) {
      errorCallback(socket, "user/get", data, error);
    }
  });

  socket.on("user/new", async (data, messageId, callback) => {
    try {
      const user = new User(data);
      await user.save();
      const users = await User.find({}).select("name role");

      io.emit("user/getAllUpdate", { data: users });

      dataCallback(callback, user, messageId);
    } catch (error) {
      errorCallback(socket, "user/new", data, error, messageId);
    }
  });

  socket.on("user/changePassword", async (data, callback) => {
    try {
      const id = data._id;
      const oldPassword = data.oldPassword;
      const newPassword = data.newPassword;
      provideCheck(id, "id");
      provideCheck(oldPassword, "oldPassword");
      provideCheck(newPassword, "newPassword");

      const user = await User.findById(data._id);
      existCheck(user, "User");

      const isPasswordCorrect = await user.checkPassword(oldPassword);
      customCheck(isPasswordCorrect, "Wrong password!");
      user.password = newPassword;

      await user.save();

      noDataCallback(callback);
    } catch (error) {
      errorCallback(socket, "user/changePassword", data, error);
    }
  });

  socket.on("user/adminChangePassword", async (data, callback) => {
    try {
      const id = data._id;
      const newPassword = data.newPassword;

      provideCheck(newPassword, "newPassword");

      const user = await User.findById(data._id);
      existCheck(user, "User");

      user.password = newPassword;

      await user.save();

      noDataCallback(callback);
    } catch (error) {
      errorCallback(socket, "user/adminChangePassword", data, error);
    }
  });

  socket.on("user/changeRole", async (data, callback) => {
    try {
      const id = data._id;
      const newRole = data.newRole;
      provideCheck(id, "id");
      provideCheck(newRole, "newRole");

      const user = await User.findById(data._id);
      existCheck(user, "User");

      user.role = newRole;
      await user.save();

      const users = await User.find({}).select("name role");
      io.emit("user/getAllUpdate", { data: users });

      dataCallback(callback, user);
    } catch (error) {
      errorCallback(socket, "user/changeRole", data, error);
    }
  });

  socket.on("user/getRoles", async (data, callback) => {
    try {
      const roles = User.getRoles();

      dataCallback(callback, roles);
    } catch (error) {
      errorCallback(socket, "user/getRoles", data, error);
    }
  });

  socket.on("user/delete", async (data, messageId, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      await User.deleteOne({ _id: id });

      const users = await User.find({}).select("name role");
      io.emit("user/getAllUpdate", { data: users });

      noDataCallback(callback, messageId);
    } catch (error) {
      errorCallback(socket, "user/delete", data, error, messageId);
    }
  });
};

module.exports = socketUserHandler;
