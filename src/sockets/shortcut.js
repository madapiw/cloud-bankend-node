const Shortcut = require("../models/shortcut");

const {
  dataCallback,
  errorCallback,
  noDataCallback,
} = require("../utils/socketCallbacks");
const {
  provideCheck,
  existCheck,
  customCheck,
} = require("../utils/socketDataCheck");
const { updateCheck } = require("../utils/socketUpdateCheck");

const socketShortcutHandler = (io, socket) => {
  socket.on("shortcut/getAll", async (data, callback) => {
    try {
      const shortcuts = await Shortcut.find({});

      dataCallback(callback, shortcuts);
    } catch (error) {
      errorCallback(socket, "shortcut/getAll", {}, error);
    }
  });

  socket.on("shortcut/new", async (data, messageId, callback) => {
    try {
      const shortcut = new Shortcut(data);
      await shortcut.save();
      const shortcuts = await Shortcut.find({});

      io.emit("shortcut/getAllUpdate", { data: shortcuts });

      dataCallback(callback, shortcut, messageId);
    } catch (error) {
      errorCallback(socket, "shortcut/new", data, error, messageId);
    }
  });

  socket.on("shortcut/update", async (data, messageId, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const shortcut = await Shortcut.findById(id);
      existCheck(shortcut, "Shortcut");

      updateCheck(shortcut, data, "shortcut");
      updateCheck(shortcut, data, "type");

      await shortcut.save();

      const shortcuts = await Shortcut.find({});

      io.emit("shortcut/getAllUpdate", { data: shortcuts });

      dataCallback(callback, shortcut, messageId);
    } catch (error) {
      errorCallback(socket, "shortcut/update", data, error, messageId);
    }
  });

  socket.on("shortcut/getTypes", async (data, callback) => {
    try {
      const types = Shortcut.getTypes();

      dataCallback(callback, types);
    } catch (error) {
      errorCallback(socket, "shortcut/getTypes", data, error);
    }
  });

  socket.on("shortcut/delete", async (data, messageId, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      await Shortcut.deleteOne({ _id: id });

      const shortcuts = await Shortcut.find({});
      io.emit("shortcut/getAllUpdate", { data: shortcuts });

      noDataCallback(callback, messageId);
    } catch (error) {
      errorCallback(socket, "shortcut/delete", data, error, messageId);
    }
  });
};

module.exports = socketShortcutHandler;
