const Tag = require("../models/tag");

const {
  dataCallback,
  errorCallback,
  noDataCallback,
} = require("../utils/socketCallbacks");
const { provideCheck, existCheck } = require("../utils/socketDataCheck");
const { updateCheck } = require("../utils/socketUpdateCheck");

const socketTagHandler = (io, socket) => {
  socket.on("tag/getAll", async (data, callback) => {
    try {
      const tags = await Tag.find({});

      dataCallback(callback, tags);
    } catch (error) {
      errorCallback(socket, "tag/getAll", {}, error);
    }
  });

  socket.on("tag/get", async (data, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const tag = await Tag.findById(id);
      existCheck(tag, "Tag");

      dataCallback(callback, tag);
    } catch (error) {
      errorCallback(socket, "tag/get", data, error);
    }
  });

  socket.on("tag/getTypes", async (data, callback) => {
    try {
      const types = await Tag.getTypes();

      dataCallback(callback, types);
    } catch (error) {
      errorCallback(socket, "tag/getTypes", {}, error);
    }
  });

  socket.on("tag/new", async (data, messageId, callback) => {
    try {
      const tag = new Tag(data);
      await tag.save();
      const tags = await Tag.find({});

      const types = await Tag.getTypes();

      io.emit("tag/getAllUpdate", { data: tags });
      io.emit("tag/getTypesUpdate", { data: types });

      dataCallback(callback, tag, messageId);
    } catch (error) {
      errorCallback(socket, "tag/new", data, error, messageId);
    }
  });

  socket.on("tag/update", async (data, messageId, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const tag = await Tag.findById(id);
      existCheck(tag, "Tag");

      updateCheck(tag, data, "type");
      updateCheck(tag, data, "show");
      updateCheck(tag, data, "color");

      await tag.save();
      const tags = await Tag.find({});

      const types = await Tag.getTypes();

      io.emit("tag/getAllUpdate", { data: tags });
      io.emit("tag/getTypesUpdate", { data: types });

      dataCallback(callback, tag, messageId);
    } catch (error) {
      errorCallback(socket, "tag/update", data, error, messageId);
    }
  });

  socket.on("tag/delete", async (data, messageId, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      await Tag.deleteOne({ _id: id });
      const tags = await Tag.find({});

      const types = await Tag.getTypes();

      io.emit("tag/getAllUpdate", { data: tags });
      io.emit("tag/getTypesUpdate", { data: types });

      noDataCallback(callback, messageId);
    } catch (error) {
      errorCallback(socket, "tag/delete", data, error, messageId);
    }
  });
};

module.exports = socketTagHandler;
