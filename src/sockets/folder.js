const { get, uniq } = require("lodash");
const Log = require("../models/log");
const Folder = require("../models/folder");
const Tag = require("../models/tag");
const Person = require("../models/person");
const { foldersQueryParser } = require("../utils/queryParser");

const {
  dataCallback,
  errorCallback,
  noDataCallback,
} = require("../utils/socketCallbacks");
const { provideCheck, existCheck } = require("../utils/socketDataCheck");
const { updateCheck } = require("../utils/socketUpdateCheck");
//ADD GETTING BY FILTERS AND PAGINATION

const socketFolderHandler = (io, socket) => {
  socket.on("folder/new", async (data, messageId, callback) => {
    try {
      const folder = new Folder(data);
      await folder.save();

      io.emit("folder/askForUpdate");
      io.emit("folder/addSelectedLogs", {
        folderId: folder._id,
        userId: data.user,
      });

      dataCallback(callback, folder, messageId);
    } catch (error) {
      errorCallback(socket, "folder/new", data, error, messageId);
    }
  });

  socket.on("folder/get", async (data, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const folder = await Folder.findById(id)
        .populate("user", "name")
        .populate("tags", ["color", "name"])
        .populate("persons", "name")
        .populate("logsTags", ["color", "name"])
        .populate("logsPersons", "name")
        .populate({
          path: "notes",
          populate: { path: "user", select: "name" },
        });
      existCheck(folder, "Folder");

      folder.logsTags = uniq(folder.logsTags);
      folder.logsPersons = uniq(folder.logsPersons);

      dataCallback(callback, folder);
    } catch (error) {
      errorCallback(socket, "folder/get", data, error);
    }
  });

  socket.on("folder/getForOverview", async (data, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const folder = await Folder.findById(id)
        .populate("user", "name")
        .populate("tags", ["color", "name"])
        .populate("persons", "name")
        .populate("logsTags", ["color", "name"])
        .populate("logsPersons", "name")
        .populate({
          path: "notes",
          populate: { path: "user", select: "name" },
        })
        .populate({
          path: "logs",
          populate: [
            { path: "user", select: "name" },
            { path: "tags", select: ["color", "name"] },
            { path: "persons", select: "name" },
            { path: "folders", select: "name" },
          ],
        });
      existCheck(folder, "Folder");

      folder.logsTags = uniq(folder.logsTags);
      folder.logsPersons = uniq(folder.logsPersons);

      dataCallback(callback, folder);
    } catch (error) {
      errorCallback(socket, "folder/get", data, error);
    }
  });

  socket.on("folder/getAll", async (data, callback) => {
    try {
      const folders = await Folder.find({})
        .populate("user", "name")
        .populate("tags", ["color", "name"])
        .populate("persons", "name");

      dataCallback(callback, folders);
    } catch (error) {
      errorCallback(socket, "folder/get", data, error);
    }
  });

  socket.on("folder/getUser", async (data, callback) => {
    try {
      const userId = data.userId;
      provideCheck(userId, "userId");

      const folders = await Folder.find({ user: userId })
        .populate("user", "name")
        .populate("tags", ["color", "name"])
        .populate("persons", "name");

      dataCallback(callback, folders);
    } catch (error) {
      errorCallback(socket, "folder/getUser", data, error);
    }
  });

  socket.on("folder/stars", async (data, messageId, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const folder = await Folder.findById(id);
      existCheck(folder, "Folder");

      const stars = get(data, "stars", 0);
      folder.stars = stars;
      await folder.save();

      io.emit("folder/askForUpdate");

      dataCallback(callback, folder, messageId);
    } catch (error) {
      errorCallback(socket, "folder/stars", data, error, messageId);
    }
  });

  socket.on("folder/like", async (data, messageId, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const folder = await Folder.findById(id);
      existCheck(folder, "Folder");

      const like = get(data, "like", false);
      folder.like = like;
      await folder.save();

      io.emit("folder/askForUpdate");

      dataCallback(callback, folder, messageId);
    } catch (error) {
      errorCallback(socket, "folder/like", data, error, messageId);
    }
  });

  socket.on("folder/addTagToMultiple", async (data, messageId, callback) => {
    try {
      const folders = data.folders;
      const tagId = data.tagId;
      provideCheck(folders, "folders");
      provideCheck(tagId, "tagId");

      await Folder.updateMany(
        { $and: [{ _id: { $in: folders } }, { tags: { $ne: tagId } }] },
        { $push: { tags: tagId } }
      );

      io.emit("folder/askForUpdate");

      dataCallback(callback, folders, messageId);
    } catch (error) {
      errorCallback(socket, "folder/addTagToMultiple", data, error, messageId);
    }
  });

  socket.on("folder/addTag", async (data, messageId, callback) => {
    try {
      const id = data._id;
      const tagId = data.tagId;
      provideCheck(id, "id");
      provideCheck(tagId, "tagId");

      const folder = await Folder.findById(id);
      existCheck(folder, "Folder");

      const tag = await Tag.findById(tagId);
      existCheck(tag, "Tag");

      if (folder.tags.includes(tagId)) {
        return dataCallback(callback, folder, messageId);
      }

      folder.tags.push(tagId);

      await folder.save();

      io.emit("folder/askForUpdate");
      dataCallback(callback, folder, messageId);
    } catch (error) {
      errorCallback(socket, "folder/addTag", data, error, messageId);
    }
  });

  socket.on("folder/removeTag", async (data, messageId, callback) => {
    try {
      const id = data._id;
      const tagId = data.tagId;
      provideCheck(id, "id");
      provideCheck(tagId, "tagId");

      const folder = await Folder.findById(id);
      existCheck(folder, "Folder");

      folder.tags = folder.tags.filter((elem) => elem.toString() !== tagId);

      await folder.save();

      io.emit("folder/askForUpdate");
      dataCallback(callback, folder, messageId);
    } catch (error) {
      errorCallback(socket, "folder/removeTag", data, error, messageId);
    }
  });

  socket.on("folder/addPersonToMultiple", async (data, messageId, callback) => {
    try {
      const folders = data.folders;
      const personId = data.personId;
      provideCheck(folders, "folders");
      provideCheck(personId, "personId");

      await Folder.updateMany(
        { $and: [{ _id: { $in: folders } }, { persons: { $ne: personId } }] },
        { $push: { persons: personId } }
      );

      io.emit("folder/askForUpdate");

      dataCallback(callback, folders, messageId);
    } catch (error) {
      errorCallback(
        socket,
        "folder/addPersonToMultiple",
        data,
        error,
        messageId
      );
    }
  });

  socket.on("folder/addPerson", async (data, messageId, callback) => {
    try {
      const id = data._id;
      const personId = data.personId;
      provideCheck(id, "id");
      provideCheck(personId, "personId");

      const folder = await Folder.findById(id);
      existCheck(folder, "Folder");

      const person = await Person.findById(personId);
      existCheck(person, "Person");

      if (folder.persons.includes(personId)) {
        return dataCallback(callback, folder, messageId);
      }

      folder.persons.push(personId);

      await folder.save();

      io.emit("folder/askForUpdate");
      dataCallback(callback, folder, messageId);
    } catch (error) {
      errorCallback(socket, "folder/addPerson", data, error, messageId);
    }
  });

  socket.on("folder/removePerson", async (data, messageId, callback) => {
    try {
      const id = data._id;
      const personId = data.personId;
      provideCheck(id, "id");
      provideCheck(personId, "personId");

      const folder = await Folder.findById(id);
      existCheck(folder, "Folder");

      folder.persons = folder.persons.filter(
        (elem) => elem.toString() !== personId
      );
      await folder.save();

      io.emit("folder/askForUpdate");

      dataCallback(callback, folder, messageId);
    } catch (error) {
      errorCallback(socket, "folder/removePerson", data, error, messageId);
    }
  });

  socket.on("folder/update", async (data, messageId, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const folder = await Folder.findById(id);
      existCheck(folder, "Folder");

      updateCheck(folder, data, "name");
      updateCheck(folder, data, "dateStart");
      updateCheck(folder, data, "dateEnd");
      updateCheck(folder, data, "tags");
      updateCheck(folder, data, "persons");

      await folder.save();

      io.emit("folder/askForUpdate");

      dataCallback(callback, folder, messageId);
    } catch (error) {
      errorCallback(socket, "folder/update", data, error, messageId);
    }
  });

  socket.on("folder/delete", async (data, messageId, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      await Folder.deleteOne({ _id: id });

      io.emit("folder/askForUpdate");
      io.emit("log/askForUpdate", { userId: data.userId });

      noDataCallback(callback, messageId);
    } catch (error) {
      errorCallback(socket, "folder/delete", data, error, messageId);
    }
  });

  socket.on("folder/getFiltered", async (data, callback) => {
    try {
      const page = data.page || 0;
      const perPage = data.MAX_LOGS_PER_PAGE || 0;
      const query = foldersQueryParser(data);

      const folders = await Folder.find(query)
        .sort({ dateStart: -1 })
        .skip(page * perPage)
        .limit(perPage)
        .populate("user", "name")
        .populate("tags", ["color", "name"])
        .populate("persons", "name");

      dataCallback(callback, folders);
    } catch (error) {
      errorCallback(socket, "folder/getFiltered", data, error);
    }
  });
};

module.exports = socketFolderHandler;
