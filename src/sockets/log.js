const { get } = require("lodash");
const Log = require("../models/log");
const Folder = require("../models/folder");
const Tag = require("../models/tag");
const Person = require("../models/person");
const { logsQueryParser } = require("../utils/queryParser");

const {
  dataCallback,
  errorCallback,
  noDataCallback,
} = require("../utils/socketCallbacks");
const { provideCheck, existCheck } = require("../utils/socketDataCheck");
const { updateCheck } = require("../utils/socketUpdateCheck");
//ADD GETTING BY FILTERS AND PAGINATION

const socketLogHandler = (io, socket) => {
  socket.on("log/new", async (data, messageId, callback) => {
    try {
      const log = new Log(data);
      await log.save();

      await Folder.updateMany(
        { _id: { $in: log.folders } },
        { $push: { logsTags: log.tags } }
      );

      await Folder.updateMany(
        { _id: { $in: log.folders } },
        { $push: { logsPersons: log.persons } }
      );

      await Folder.updateMany(
        { _id: { $in: log.folders } },
        { dateEnd: log.dateEnd, dayEnd: log.day }
      );

      await Folder.updateMany(
        {
          $and: [{ _id: { $in: log.folders }, dateStart: { $exists: false } }],
        },
        { dateStart: log.dateStart, dayStart: log.day }
      );

      io.emit("log/askForUpdate", { userId: data.userId });

      dataCallback(callback, log, messageId);
    } catch (error) {
      errorCallback(socket, "log/new", data, error, messageId);
    }
  });

  socket.on("log/get", async (data, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const log = await Log.findById(id)
        .populate("user", "name")
        .populate("tags", ["color", "name"])
        .populate("persons", "name")
        .populate("folders", "name");
      existCheck(log, "Log");

      dataCallback(callback, log);
    } catch (error) {
      errorCallback(socket, "log/get", data, error);
    }
  });

  socket.on("log/getAll", async (data, callback) => {
    try {
      const logs = await Log.find({})
        .sort({ dateStart: -1 })
        .populate("user", "name")
        .populate("tags", ["color", "name"])
        .populate("persons", "name")
        .populate("folders", "name");

      dataCallback(callback, logs);
    } catch (error) {
      errorCallback(socket, "log/getAll", data, error);
    }
  });

  socket.on("log/getUser", async (data, callback) => {
    try {
      const userId = data.userId;
      const perPage = data.perPage || 20;
      const page = data.page;
      provideCheck(userId, "userId");

      const logs = await Log.find({ user: userId })
        .sort({ dateStart: -1 })
        .skip(page * perPage)
        .limit(perPage)
        .populate("user", "name")
        .populate("tags", ["color", "name"])
        .populate("persons", "name")
        .populate("folders", "name");

      dataCallback(callback, logs);
    } catch (error) {
      errorCallback(socket, "log/getUser", data, error);
    }
  });

  socket.on("log/stars", async (data, messageId, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const log = await Log.findById(id);
      existCheck(log, "Log");

      const stars = get(data, "stars", 0);
      log.stars = stars;
      await log.save();

      io.emit("log/askForUpdate", { userId: data.userId });

      dataCallback(callback, log, messageId);
    } catch (error) {
      errorCallback(socket, "log/stars", data, error, messageId);
    }
  });

  socket.on("log/like", async (data, messageId, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const log = await Log.findById(id);
      existCheck(log, "Log");

      const like = get(data, "like", false);
      log.like = like;
      await log.save();

      io.emit("log/askForUpdate", { userId: data.userId });

      dataCallback(callback, log, messageId);
    } catch (error) {
      errorCallback(socket, "log/like", data, error, messageId);
    }
  });

  socket.on("log/addTagToMultiple", async (data, messageId, callback) => {
    try {
      const logs = data.logs;
      const tagId = data.tagId;
      provideCheck(logs, "logs");
      provideCheck(tagId, "tagId");

      const tag = await Tag.findById(tagId);
      existCheck(tag, "Tag");

      //list of folders
      const foundedFolders = await Log.find({
        $and: [{ _id: { $in: logs } }, { tags: { $ne: tagId } }],
      }).select("folders -_id");

      await Log.updateMany(
        { $and: [{ _id: { $in: logs } }, { tags: { $ne: tagId } }] },
        { $push: { tags: tagId } }
      );

      //add tags to each folder
      foundedFolders.forEach(async (elem) => {
        await Folder.updateMany(
          { _id: { $in: elem.folders } },
          { $push: { logsTags: tagId } }
        );
      });

      io.emit("log/askForUpdate", { userId: data.userId });

      dataCallback(callback, logs, messageId);
    } catch (error) {
      errorCallback(socket, "log/addTagToMultiple", data, error, messageId);
    }
  });

  socket.on("log/addTag", async (data, messageId, callback) => {
    try {
      const id = data._id;
      const tagId = data.tagId;
      provideCheck(id, "id");
      provideCheck(tagId, "tagId");

      const log = await Log.findById(id);
      existCheck(log, "Log");

      if (log.tags.includes(tagId)) {
        return dataCallback(callback, log, messageId);
      }

      const tag = await Tag.findById(tagId);
      existCheck(tag, "Tag");

      log.tags.push(tagId);

      await log.save();

      //add tag to each folder
      await Folder.updateMany(
        { _id: { $in: log.folders } },
        { $push: { logsTags: tagId } }
      );

      io.emit("log/askForUpdate", { userId: data.userId });
      dataCallback(callback, log, messageId);
    } catch (error) {
      errorCallback(socket, "log/addTag", data, error, messageId);
    }
  });

  socket.on("log/removeTag", async (data, messageId, callback) => {
    try {
      const id = data._id;
      const tagId = data.tagId;
      provideCheck(id, "id");
      provideCheck(tagId, "tagId");

      const log = await Log.findById(id);
      existCheck(log, "Log");

      log.tags = log.tags.filter((elem) => elem.toString() !== tagId);

      await log.save();

      const folders = await Folder.find({ _id: { $in: log.folders } });

      //remove tag from each folder
      folders.forEach(async (folder) => {
        const removeIndex = folder.logsTags.findIndex(
          (elem) => elem.toString() === tagId
        );

        if (removeIndex !== -1) {
          folder.logsTags.splice(removeIndex, 1);
          await folder.save();
        }
      });

      io.emit("log/askForUpdate", { userId: data.userId });

      dataCallback(callback, log, messageId);
    } catch (error) {
      errorCallback(socket, "log/removeTag", data, error, messageId);
    }
  });

  socket.on("log/addPersonToMultiple", async (data, messageId, callback) => {
    try {
      const logs = data.logs;
      const personId = data.personId;
      provideCheck(logs, "logs");
      provideCheck(personId, "personId");

      const person = await Person.findById(personId);
      existCheck(person, "Person");

      const foundedFolders = await Log.find({
        $and: [{ _id: { $in: logs } }, { persons: { $ne: personId } }],
      }).select("folders -_id");

      await Log.updateMany(
        { $and: [{ _id: { $in: logs } }, { persons: { $ne: personId } }] },
        { $push: { persons: personId } }
      );

      //add person to each folder
      foundedFolders.forEach(async (elem) => {
        await Folder.updateMany(
          { _id: { $in: elem.folders } },
          { $push: { logsPersons: personId } }
        );
      });

      io.emit("log/askForUpdate", { userId: data.userId });

      dataCallback(callback, logs, messageId);
    } catch (error) {
      errorCallback(socket, "log/addPersonToMultiple", data, error, messageId);
    }
  });

  socket.on("log/addPerson", async (data, messageId, callback) => {
    try {
      const id = data._id;
      const personId = data.personId;
      provideCheck(id, "id");
      provideCheck(personId, "personId");

      const log = await Log.findById(id);
      existCheck(log, "Log");

      if (log.persons.includes(personId)) {
        return dataCallback(callback, log, messageId);
      }

      log.persons.push(personId);

      await log.save();

      //add person to each folder
      await Folder.updateMany(
        { _id: { $in: log.folders } },
        { $push: { logsPersons: personId } }
      );

      io.emit("log/askForUpdate", { userId: data.userId });

      dataCallback(callback, log, messageId);
    } catch (error) {
      errorCallback(socket, "log/addPerson", data, error, messageId);
    }
  });

  socket.on("log/removePerson", async (data, messageId, callback) => {
    try {
      const id = data._id;
      const personId = data.personId;
      provideCheck(id, "id");
      provideCheck(personId, "personId");

      const log = await Log.findById(id);
      existCheck(log, "Log");

      log.persons = log.persons.filter((elem) => elem.toString() !== personId);
      await log.save();

      const folders = await Folder.find({ _id: { $in: log.folders } });

      //remove person from each folder
      folders.forEach(async (folder) => {
        const removeIndex = folder.logsPersons.findIndex(
          (elem) => elem.toString() === personId
        );

        if (removeIndex !== -1) {
          folder.logsPersons.splice(removeIndex, 1);
          await folder.save();
        }
      });

      io.emit("log/askForUpdate", { userId: data.userId });

      dataCallback(callback, log, messageId);
    } catch (error) {
      errorCallback(socket, "log/removePerson", data, error, messageId);
    }
  });

  socket.on("log/addFolderToMultiple", async (data, messageId, callback) => {
    try {
      const logs = data.logs;
      const folderId = data.folderId;
      provideCheck(logs, "logs");
      provideCheck(folderId, "folderId");

      const logForFolderUpdate = await Log.find({
        $and: [{ _id: { $in: logs } }, { folders: { $ne: folderId } }],
      });

      await Log.updateMany(
        { $and: [{ _id: { $in: logs } }, { folders: { $ne: folderId } }] },
        { $push: { folders: folderId } }
      );

      const folderToModify = await Folder.findById(folderId);

      const startDateLog = await Log.findOne({
        folders: { $eq: folderId },
      }).sort({ dateStart: 1 });
      const endDateLog = await Log.findOne({
        folders: { $eq: folderId },
      }).sort({ dateEnd: -1 });

      folderToModify.dateStart = startDateLog.dateStart;
      folderToModify.dayStart = startDateLog.day;
      folderToModify.dateEnd = endDateLog.dateEnd;
      folderToModify.dayEnd = endDateLog.day;

      //add tags and persons to the folder
      await Promise.all(
        logForFolderUpdate.map(async (elem, index, array) => {
          folderToModify.logsTags = folderToModify.logsTags.concat(elem.tags);
          folderToModify.logsPersons = folderToModify.logsPersons.concat(
            elem.persons
          );

          if (index === array.length - 1) {
            await folderToModify.save();
          }
        })
      );

      io.emit("log/askForUpdate", { userId: data.userId });
      //io.emit("folder/askForUpdate");

      dataCallback(callback, logs, messageId);
    } catch (error) {
      errorCallback(socket, "log/addFolderToMultiple", data, error, messageId);
    }
  });

  socket.on("log/addFolder", async (data, messageId, callback) => {
    try {
      const id = data._id;
      const folderId = data.folderId;
      provideCheck(id, "id");
      provideCheck(folderId, "folderId");

      const log = await Log.findById(id);
      existCheck(log, "Log");

      if (log.folders.includes(folderId)) {
        return dataCallback(callback, log, messageId);
      }

      const folder = await Folder.findById(folderId);
      existCheck(folder, "Folder");

      log.folders.push(folderId);

      await log.save();

      const folderToModify = await Folder.findById(folderId);

      const startDateLog = await Log.findOne({
        folders: { $eq: folderId },
      }).sort({ dateStart: 1 });
      const endDateLog = await Log.findOne({
        folders: { $eq: folderId },
      }).sort({ dateEnd: -1 });

      folderToModify.dateStart = startDateLog.dateStart;
      folderToModify.dayStart = startDateLog.day;
      folderToModify.dateEnd = endDateLog.dateEnd;
      folderToModify.dayEnd = endDateLog.day;
      //add tags and persons to the folder
      folderToModify.logsTags = folderToModify.logsTags.concat(log.tags);
      folderToModify.logsPersons = folderToModify.logsPersons.concat(
        log.persons
      );

      await folderToModify.save();

      io.emit("log/askForUpdate", { userId: data.userId });
      //io.emit("folder/askForUpdate");

      dataCallback(callback, log, messageId);
    } catch (error) {
      errorCallback(socket, "log/addFolder", data, error, messageId);
    }
  });

  socket.on("log/removeFolder", async (data, messageId, callback) => {
    try {
      const id = data._id;
      const folderId = data.folderId;
      provideCheck(id, "id");
      provideCheck(folderId, "folderId");

      const log = await Log.findById(id);
      existCheck(log, "Log");

      log.folders = log.folders.filter((elem) => elem.toString() !== folderId);
      await log.save();

      const folderToModify = await Folder.findById(folderId);

      const startDateLog = await Log.findOne({
        folders: { $eq: folderId },
      }).sort({ dateStart: 1 });
      const endDateLog = await Log.findOne({
        folders: { $eq: folderId },
      }).sort({ dateEnd: -1 });

      folderToModify.dateStart = get(startDateLog, "dateStart", undefined);
      folderToModify.dayStart = get(startDateLog, "day", undefined);
      folderToModify.dateEnd = get(endDateLog, "dateEnd", undefined);
      folderToModify.dayEnd = get(endDateLog, "day", undefined);

      //remove tags and persons from the folder
      log.tags.forEach((tag) => {
        const removeIndex = folderToModify.logsTags.findIndex(
          (elem) => elem.toString() === tag.toString()
        );

        if (removeIndex !== -1) {
          folderToModify.logsTags.splice(removeIndex, 1);
        }
      });

      log.persons.forEach((person) => {
        const removeIndex = folderToModify.logsPersons.findIndex(
          (elem) => elem.toString() === person.toString()
        );

        if (removeIndex !== -1) {
          folderToModify.logsPersons.splice(removeIndex, 1);
        }
      });

      await folderToModify.save();

      io.emit("log/askForUpdate", { userId: data.userId });
      //io.emit("folder/askForUpdate");

      dataCallback(callback, log, messageId);
    } catch (error) {
      errorCallback(socket, "log/removeFolder", data, error, messageId);
    }
  });

  socket.on("log/update", async (data, messageId, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const log = await Log.findById(id);
      existCheck(log, "Log");

      updateCheck(log, data, "text");
      updateCheck(log, data, "dateStart");
      updateCheck(log, data, "dateEnd");
      updateCheck(log, data, "tags");
      updateCheck(log, data, "persons");

      await log.save();

      const folders = log.folders;

      await Promise.all(
        folders.map(async (elem) => {
          const folderToModify = await Folder.findById(elem);

          if (folderToModify) {
            const startDateLog = await Log.findOne({
              folders: { $eq: elem },
            }).sort({ dateStart: 1 });
            const endDateLog = await Log.findOne({
              folders: { $eq: elem },
            }).sort({ dateEnd: -1 });

            folderToModify.dateStart = get(
              startDateLog,
              "dateStart",
              undefined
            );
            folderToModify.dayStart = get(startDateLog, "day", undefined);
            folderToModify.dateEnd = get(endDateLog, "dateEnd", undefined);
            folderToModify.dayEnd = get(endDateLog, "day", undefined);

            await folderToModify.save();
          }
        })
      );

      io.emit("log/askForUpdate", { userId: data.userId });

      dataCallback(callback, log, messageId);
    } catch (error) {
      errorCallback(socket, "log/update", data, error, messageId);
    }
  });

  socket.on("log/delete", async (data, messageId, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const logToDelete = await Log.findById(id);
      const tags = logToDelete.tags;
      const persons = logToDelete.persons;
      const folders = logToDelete.folders;

      await Log.deleteOne({ _id: id });

      await Promise.all(
        folders.map(async (elem) => {
          const folderToModify = await Folder.findById(elem);

          if (folderToModify) {
            tags.forEach((tag) => {
              const removeIndex = folderToModify.logsTags.findIndex(
                (elem) => elem.toString() === tag.toString()
              );

              if (removeIndex !== -1) {
                folderToModify.logsTags.splice(removeIndex, 1);
              }
            });

            persons.forEach((person) => {
              const removeIndex = folderToModify.logsPersons.findIndex(
                (elem) => elem.toString() === person.toString()
              );

              if (removeIndex !== -1) {
                folderToModify.logsPersons.splice(removeIndex, 1);
              }
            });

            const startDateLog = await Log.findOne({
              folders: { $eq: elem },
            }).sort({ dateStart: 1 });
            const endDateLog = await Log.findOne({
              folders: { $eq: elem },
            }).sort({ dateEnd: -1 });

            folderToModify.dateStart = get(
              startDateLog,
              "dateStart",
              undefined
            );
            folderToModify.dayStart = get(startDateLog, "day", undefined);
            folderToModify.dateEnd = get(endDateLog, "dateEnd", undefined);
            folderToModify.dayEnd = get(endDateLog, "day", undefined);

            await folderToModify.save();
          }
        })
      );

      io.emit("log/askForUpdate", { userId: data.userId });

      noDataCallback(callback, messageId);
    } catch (error) {
      errorCallback(socket, "log/delete", data, error, messageId);
    }
  });

  socket.on("log/getFiltered", async (data, callback) => {
    try {
      const page = data.page || 0;
      const perPage = data.MAX_LOGS_PER_PAGE || 0;
      const query = logsQueryParser(data);

      const logs = await Log.find(query)
        .sort({ dateStart: -1 })
        .skip(page * perPage)
        .limit(perPage)
        .populate("user", "name")
        .populate("tags", ["color", "name"])
        .populate("persons", "name")
        .populate("folders", "name");

      dataCallback(callback, logs);
    } catch (error) {
      errorCallback(socket, "log/getFiltered", data, error);
    }
  });

  socket.on("log/getFilteredForOverview", async (data, callback) => {
    try {
      const query = logsQueryParser(data);
      const folder = {};

      const logs = await Log.find(query)
        .sort({ dateStart: 1 })
        .populate("user", "name")
        .populate("tags", ["color", "name"])
        .populate("persons", "name")
        .populate("folders", "name");

      if (logs.length === 0) {
        return dataCallback(callback, { logs: [] });
      }

      const tags = await Tag.find({ _id: data.tagsFilters });
      const persons = await Person.find({ _id: data.personsFilters });

      folder.name = "Folder generated automatically";
      folder.logs = logs;
      folder.dateStart = logs[0].dateStart;
      folder.dayStart = logs[0].day;
      folder.dateEnd = logs[logs.length - 1].dateEnd;
      folder.dayEnd = logs[logs.length - 1].day;
      folder.logsPersons = [];
      folder.logsTags = [];
      folder.notes = [];
      folder.persons = persons;
      folder.tags = tags;
      folder.user = { _id: "", name: "" };

      dataCallback(callback, folder);
    } catch (error) {
      errorCallback(socket, "log/getFilteredForOverview", data, error);
    }
  });
};

module.exports = socketLogHandler;
