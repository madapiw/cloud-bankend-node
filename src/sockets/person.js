const Person = require("../models/person");

const {
  dataCallback,
  errorCallback,
  noDataCallback,
} = require("../utils/socketCallbacks");
const { provideCheck, existCheck } = require("../utils/socketDataCheck");
const { updateCheck } = require("../utils/socketUpdateCheck");

const socketPersonHandler = (io, socket) => {
  socket.on("person/getAll", async (data, callback) => {
    try {
      const persons = await Person.find({});

      dataCallback(callback, persons);
    } catch (error) {
      errorCallback(socket, "person/getAll", {}, error);
    }
  });

  socket.on("person/get", async (data, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const person = await Person.findById(id);
      existCheck(person, "Person");

      dataCallback(callback, person);
    } catch (error) {
      errorCallback(socket, "person/get", data, error);
    }
  });

  socket.on("person/getTypes", async (data, callback) => {
    try {
      const types = await Person.getTypes();

      dataCallback(callback, types);
    } catch (error) {
      errorCallback(socket, "person/getTypes", {}, error);
    }
  });

  socket.on("person/new", async (data, messageId, callback) => {
    try {
      const person = new Person(data);
      await person.save();
      const persons = await Person.find({});

      const types = await Person.getTypes();

      io.emit("person/getTypesUpdate", { data: types });
      io.emit("person/getAllUpdate", { data: persons });

      dataCallback(callback, person, messageId);
    } catch (error) {
      errorCallback(socket, "person/new", data, error, messageId);
    }
  });

  socket.on("person/update", async (data, messageId, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const person = await Person.findById(id);
      existCheck(person, "Person");

      updateCheck(person, data, "type");
      updateCheck(person, data, "show");

      await person.save();

      const persons = await Person.find({});
      const types = await Person.getTypes();

      io.emit("person/getTypesUpdate", { data: types });
      io.emit("person/getAllUpdate", { data: persons });

      dataCallback(callback, person, messageId);
    } catch (error) {
      errorCallback(socket, "person/update", data, error, messageId);
    }
  });

  socket.on("person/delete", async (data, messageId, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      await Person.deleteOne({ _id: id });
      const persons = await Person.find({});

      const types = await Person.getTypes();

      io.emit("person/getTypesUpdate", { data: types });
      io.emit("person/getAllUpdate", { data: persons });

      noDataCallback(callback, messageId);
    } catch (error) {
      errorCallback(socket, "person/delete", data, error, messageId);
    }
  });
};

module.exports = socketPersonHandler;
