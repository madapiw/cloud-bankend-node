const Note = require("../models/note");

const {
  dataCallback,
  errorCallback,
  noDataCallback,
} = require("../utils/socketCallbacks");
const { provideCheck, existCheck } = require("../utils/socketDataCheck");
const { updateCheck } = require("../utils/socketUpdateCheck");

const socketNoteHandler = (io, socket) => {
  socket.on("note/new", async (data, messageId, callback) => {
    try {
      const note = new Note(data);
      await note.save();

      dataCallback(callback, note, messageId);
    } catch (error) {
      errorCallback(socket, "note/new", data, error, messageId);
    }
  });

  socket.on("note/get", async (data, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const note = await Note.findById(id).populate("user", "name");
      existCheck(note, "Note");

      dataCallback(callback, note);
    } catch (error) {
      errorCallback(socket, "note/get", data, error);
    }
  });

  socket.on("note/getRef", async (data, callback) => {
    try {
      const refId = data.refId;
      provideCheck(refId, "refId");

      const notes = await Note.find({ reference: refId }).populate(
        "user",
        "name"
      );

      dataCallback(callback, notes);
    } catch (error) {
      errorCallback(socket, "note/getRef", data, error);
    }
  });

  socket.on("note/getUser", async (data, callback) => {
    try {
      const userId = data.userId;
      provideCheck(userId, "userId");

      const notes = await Note.find({ user: userId }).populate("user", "name");

      dataCallback(callback, notes);
    } catch (error) {
      errorCallback(socket, "note/getUser", data, error);
    }
  });

  socket.on("note/update", async (data, messageId, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      const note = await Note.findById(id);
      existCheck(note, "Note");

      updateCheck(note, data, "text");

      await note.save();

      dataCallback(callback, note, messageId);
    } catch (error) {
      errorCallback(socket, "note/update", data, error, messageId);
    }
  });

  socket.on("note/delete", async (data, messageId, callback) => {
    try {
      const id = data._id;
      provideCheck(id, "id");

      await Note.deleteOne({ _id: id });

      noDataCallback(callback, messageId);
    } catch (error) {
      errorCallback(socket, "note/delete", data, error, messageId);
    }
  });
};

module.exports = socketNoteHandler;
