//express config

const path = require("path");
const express = require("express");

const userRouter = require("../routes/user");
const personRouter = require("../routes/person");

const publicFolder = path.join(__dirname, "../public");

const app = express();

app.use(express.json());

app.use(express.static(publicFolder));
app.use(userRouter);
app.use(personRouter);

module.exports = app;
