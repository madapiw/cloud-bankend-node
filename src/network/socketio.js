//socket.io config

const http = require("http");
const socketio = require("socket.io");

const app = require("./express");
const { infoLogger } = require("../logger/logger");
const { removeUser } = require("../utils/userStorage");
const socketAuth = require("../utils/socketAuth");

const userHandler = require("../sockets/user");
const tagHandler = require("../sockets/tag");
const personHandler = require("../sockets/person");
const logHandler = require("../sockets/log");
const folderHandler = require("../sockets/folder");
const noteHandler = require("../sockets/note");
const shortcutHandler = require("../sockets/shortcut");

const server = http.createServer(app);
const io = socketio(server, {
  transports: ["websocket"],
  cors: {
    origin: true,
  },
});

io.use(socketAuth).on("connection", (socket) => {
  const id = socket.id;
  const user = socket.handshake.query.user;

  if (!user || !id) {
    console.log("No user or id!");
    socket.disconnect();
  }
  infoLogger.info(`${user} - Client connected!`);

  console.log("socket open");
  userHandler(io, socket);
  tagHandler(io, socket);
  personHandler(io, socket);
  logHandler(io, socket);
  folderHandler(io, socket);
  noteHandler(io, socket);
  shortcutHandler(io, socket);

  if (process.env.NODE_ENV !== "TEST") {
    socket.on("disconnect", () => {
      removeUser(id);
      infoLogger.info(`${user} - Client disconnected!`);
      console.log("socket closed");
    });
  }
});

module.exports = server;
